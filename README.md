# YU-GI-OH , à toi de jouer ! 

## 1- Avant propos

Nous avons mis un squelette du programme sur un git 
Voici les instructions pour le récupèrer
```bash
bels@a15p15:~$ git clone https://gitlab.com/Aitux/TPFinal.git
bels@a15p15:~$ cd dossier
```
Il ne vous reste plus qu'à importer le projet sur eclipse

## 2- Importer le projet sur eclipse

Je vous invite à regarder le diaporama à partir de la **PAGE 6** :

[Diaporama](https://fr.slideshare.net/sokngimlasy/add-eclipse-project-with-git-lab)

## 3- Les règles du jeu 

### A- Les bases

Les duellistes se saluent, puis le Duel débute par la désignation de celui qui va choisir stratégiquement s'il commence à jouer le premier tour ou s'il laisse débuter son adversaire. Cela est confié au hasard soit à l'aide d'un dé, lancé par chaque duelliste, à celui qui obtient le plus grand chiffre, soit à l'aide d'une pièce de monaie, lancée par un des duellistes, à celui qui choisit d'avance le bon côté d'un "Pile ou Face", soit en jouant à "Papier-Cailloux-Ciseaux". Le point important du choix stratégique du joueur ayant eu la chance d'être ainsi désigné est que celui qui commence le Duel ne pourra pas Attaquer son adversaire lors de ce premier tour. Pour les Duels suivants, le perdant du Duel précédent choisit qui commence.

Après avoir soigneusement mélangé son Deck et coupé soigneusement le Deck de son adversaire, chaque duelliste prend les 5 premières cartes et les tient dans sa main en présentant leur verso à leur adversaire.


###  B- Comment se déroule un tour ?

Piocher la première carte du dessus de son Deck et la mettre dans sa main. Si le duelliste ne peut pas piocher parce que son Deck est vide, il perd le Duel.

On peut Poser (en Position de Défense) ou Invoquer Normalement (en Position d'Attaque) un seul monstre, poser des Cartes Piège ou Magie à effet instantanée, activer des Cartes Magie et changer la Position de Combat d'un monstre (une seule fois par tour pour un même monstre)

C'est le moment où on peut mener toutes ses Attaques sur les monstres contrôlés par son adversaire avec les monstres en Position d'Attaque qu'on contrôle sur chaque Zone Carte Monstre de son Terrain.

Attaquer est le meilleur moyen de détruire les monstres de son adversaire et de diminuer les Points de Vie de ce dernier. Chacun des monstres en Position d'Attaque, un seul à la fois, peut Attaquer une fois durant chacun des tours un monstre adverse de son choix. La logique stratégique veut qu'on choisisse son monstre le plus fort qui Attaquera le monstre de son adversaire ayant la plus faible valeur d'Attaque afin qu'on lui impute ses Points de Vie d'un nombre maximum. Pour plus de précision, voir Positon d'Attaque contre Positon d'Attaque.

Lorsqu'on Attaque un monstre en Position de Défense face verso, son propriétaire le retourne face recto ; on peut alors le découvrir et potentiellement être surpris de subir un effet Flip. Pour plus de précision, voir Positon d'Attaque contre Positon de Défense.

Si l'adversaire ne contrôle aucun monstre sur son Terrain, on Attaque alors directement ses Points de Vie, ce qu'on nomme logiquement une Attaque directe à l'adversaire. Le calcul des dommages est simple ; il perd alors un nombre de Points de Vie égal à la valeur d'Attaque du monstre attaquant.

## 4- Aide compréhension UML


package board;

import domain.FieldMagic;
import domain.MagicTrap;
import domain.Monster;

import java.util.List;

/**
 * Project : TPFinal
 * Original Package : board
 * Created by aitux on 18/03/18.
 */
public class Side
{
    private List<Monster> monsterList;
    private List<MagicTrap> magicTrapList;
    private FieldMagic field;

    public boolean isMonsterListFull(){
        return monsterList.size() >=5;
    }

    public boolean isMagicTrapListFull(){
        return magicTrapList.size() >= 5;
    }

    public boolean isFieldUsed(){
        return field != null;
    }

    public List<Monster> getMonsterList()
    {
        return monsterList;
    }

    public void setMonsterList(List<Monster> monsterList)
    {
        this.monsterList = monsterList;
    }

    public List<MagicTrap> getMagicTrapList()
    {
        return magicTrapList;
    }

    public void setMagicTrapList(List<MagicTrap> magicTrapList)
    {
        this.magicTrapList = magicTrapList;
    }

    public FieldMagic getField()
    {
        return field;
    }

    public void setField(FieldMagic field)
    {
        this.field = field;
    }
}

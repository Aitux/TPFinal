package board;

/**
 * Project : TPFinal
 * Original Package : board
 * Created by aitux on 18/03/18.
 */
public class Board
{
    private Side playerOne;
    private Side playerTwo;

    public Board(Side playerOne, Side playerTwo)
    {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
    }

    public Side getPlayerOne()
    {
        return playerOne;
    }

    public Side getPlayerTwo()
    {
        return playerTwo;
    }
}

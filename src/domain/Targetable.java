package domain;

/**
 * Project : TPFinal
 * Original Package : domain
 * Created by aitux on 18/03/18.
 */
public interface Targetable
{
    void isTargetedByMonster(Monster m);
    void isTargetedByMagicOrTrap(MagicTrap magicTrap);
}
package domain;

import effect.Effect;

/**
 * Project : TPFinal
 * Original Package : domain
 * Created by aitux on 18/03/18.
 */
public abstract class Card implements Targetable
{
    private String name;
    private Effect effect;

    public void use(Targetable t){
        // TODO
    }



    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Effect getEffect()
    {
        return effect;
    }

    public void setEffect(Effect effect)
    {
        this.effect = effect;
    }
}

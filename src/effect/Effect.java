package effect;

import domain.Targetable;

/**
 * Project : TPFinal
 * Original Package : effect
 * Created by aitux on 18/03/18.
 */
public abstract  class Effect
{
    public abstract void activate(Targetable t);

    public abstract String desc();

    // Il faut implémenter cette classe pour créer des effets.
}

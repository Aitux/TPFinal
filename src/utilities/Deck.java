package utilities;

import domain.Card;

import java.util.List;

/**
 * Project : TPFinal
 * Original Package : utilities
 * Created by aitux on 18/03/18.
 */
public class Deck
{
    private List<Card> deck;

    public void draw(Player p, int nbCardToDraw)
    {
        // CODE A ECRIRE
        // On prend la première carte du deck on la stock temporairement dans une Card, on l'ajoute à la main du Player, puis on supprime la première carte du deck
    }

    public List<Card> getDeck()
    {
        return deck;
    }

    public void setDeck(List<Card> deck)
    {
        this.deck = deck;
    }
}

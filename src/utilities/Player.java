package utilities;

import board.Side;

/**
 * Project : TPFinal
 * Original Package : utilities
 * Created by aitux on 18/03/18.
 */
public class Player
{
    private String name;
    private int healthPoint;
    private Hand hand;
    private Graveyard gy;
    private Deck deck;
    private Side s;

    public Player(String name)
    {
        this.name = name;
        this.healthPoint = 8000;
        gy = new Graveyard();
    }

    public void draw()
    {
        deck.draw(this, 1);
    }

    public Side getS()
    {
        return s;
    }

    public void setS(Side s)
    {
        this.s = s;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getHealthPoint()
    {
        return healthPoint;
    }

    public void setHealthPoint(int healthPoint)
    {
        this.healthPoint = healthPoint;
    }

    public Hand getHand()
    {
        return hand;
    }

    public void setHand(Hand hand)
    {
        this.hand = hand;
    }

    public Graveyard getGy()
    {
        return gy;
    }

    public void setGy(Graveyard gy)
    {
        this.gy = gy;
    }

    public Deck getDeck()
    {
        return deck;
    }

    public void setDeck(Deck deck)
    {
        this.deck = deck;
    }
}

package launch;

import board.Board;
import board.Side;
import utilities.Deck;
import utilities.Player;

/**
 * Project : TPFinal
 * Original Package : launch
 * Created by aitux on 18/03/18.
 */
public class Duel
{


    public static void main(String[] args)
    {
        Duel d = new Duel();
        Side side1 = new Side();
        Side side2 = new Side();
        Board b = new Board(side1, side2);
        Player p1 = new Player("Setho");
        Player p2 = new Player("Makuba");
        p1.setDeck(d.createRandomDeck(20));
        p2.setDeck(d.createRandomDeck(20));
        p1.setS(side1);
        p2.setS(side2);

    }


    /**
     * Create a random deck.
     * @param nbCarte le nombre de carte voulu dans le Deck
     */
    public Deck createRandomDeck(int nbCarte)
    {
        // A COMPLETER

        for (int i = 0; i < nbCarte; i++)
        {
            // CREER DES CARTES ET LES AJOUTER DANS UN DECK
        }
        return null;
    }

    public void surrender(Player p)
    {
        System.err.println(p.getName() + " has surrendered.");
        System.exit(0);
    }
}
